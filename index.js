const express = require('express');
const app = express();
const port = 4000; // Vous pouvez changer le port si nécessaire

// Définir les routes
app.get('/', (req, res) => {
  res.send('Bienvenue sur mon API !');
});

// Démarrer le serveur
app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur http://localhost:${port}`);
});
